#include "serveur.h"
#include "sock_server.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

Serveur creer_serveur(int port) {
	Serveur s = (Serveur)malloc(sizeof(struct info_serveur));
	int pip_rec[2];
	int pip_em[2];
	
	// création des pipes de communication avec le serveur
	pipe(pip_rec);
	pipe(pip_em);	
	s->pipe_serveur_reception = pip_rec[0];
	s->pipe_serveur_emission = pip_em[1];

	// créatin du serveur à proprement parlé
	s->pid = fork();
	if(!s->pid) {		
		sock_server(port,pip_rec[1],pip_em[0]);
	}

	// créatin du fichier d'historique de conversation
	remove("hist");
	s->fd = open("hist",O_RDWR|O_CREAT,S_IWRITE|S_IREAD);
	s->pid_hist = -1;
	return s;
}

void fermer_serveur(Serveur * s) {
	fermer_hist(*s);
	kill((*s)->pid,9);
	free(*s);	
	*s = NULL;
}

void gerer_messages(Serveur s) {
	char buff[LONGUEUR_MESSAGE]= {0};
	
	// nous faisons l'hypothèse que la taille d'un message du serveur n'excèdera jamais LONGUEUER_MESSAGE
	read(s->pipe_serveur_reception,buff,LONGUEUR_MESSAGE);	
	
	if(strlen(buff) > 0){
		// printf("%s\n",buff+1);
		write(s->fd,buff,strlen(buff));	
	}
}

void envoyer_message_tous(Serveur s, char ** message) {
	int i = 0;
	char message_complet[1024] = {0};
	char * space = " ";

	while(message[i] != NULL) {
		strcat(message_complet,space);
		strcat(message_complet,message[i]);
		i++;
	}

	write(s->pipe_serveur_emission,message_complet, strlen(message_complet));
}

void historique(Serveur s) {
	s->pid_hist = fork();
	if(!s->pid_hist) {
		execlp("xterm","xterm","-e","./historique",NULL);
	}	
}

void fermer_hist(Serveur s) {
	if(s != NULL && s->pid_hist != -1) {
		kill(s->pid_hist,9);
		s->pid_hist = -1;
	}
}
