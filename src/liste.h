#ifndef _LISTE		//Pour �viter les d�finitions multiples

  #define _LISTE
  typedef struct chainon * Liste;
  typedef enum {INEXISTANT, ACTIF, SUSPENDU} ETAT_PROC;
  
  struct chainon {
	char* commande; // le nom de la commande
  	int pid;	// le pid du processus
	int fd;		// le "file descriptor" permettant d'obtenir l'output du processus
	ETAT_PROC etat; // l'�tat du processus
	Liste suivant;  // pointeur vers le descripteur du processus suivant
  } ;

#endif

Liste creer();

// ins�re un �l�ment en t�te de liste
int inserer_en_tete(char** commande, int pid, int fd, ETAT_PROC etat, Liste * liste);

// ins�re un �l�ment en queue de liste
int inserer_en_queue(char ** commande, int pid, int fd, ETAT_PROC etat, Liste * liste);

/* ret = rechercher(numproc, l)
   renvoie 0 si numproc n'appartient pas a la liste l,
   quelque chose de different sinon */
int rechercher(int x, Liste l);

/* ret = modifier(numproc, etat, l)
   modifie l'etat de numproc s'il appartient a la liste
   renvoie 0 si numproc n'est pas dans la liste */
int modifier(int x, ETAT_PROC etat,Liste l);

/* afficher(l1, mess) affiche les elements de l 
mess est un tableau de pointeurs sur les chaines correspondant aux etats. si mess==NULL, c'est le code de l'etat qui est affiche*/
void afficher(Liste l);

// lit l'�tat de processus de pid "pid"
ETAT_PROC lire_etat_proc(int pid, Liste liste);

// le file descriptor permettant d'obtenir l'output du processus fils de pid "pid"
int lire_fd_proc(int pid, Liste liste);

/* supprimer(numproc, l1)
- supprime le processus numproc de la liste l1
- renvoie 0 si numproc n'appartient pas a la liste*/
int supprimer_proc(int x, Liste *l);

/* detruire(l1) d�truit la liste l1 */
void detruire(Liste *l);
