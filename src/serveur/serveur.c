#include "serveur.h"
#include "sock_server.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>

void ajouter_message_hist(Serveur s,char * message);
void afficher_hist(Serveur s);

Serveur creer_serveur(int port) {
	Serveur s = (Serveur)malloc(sizeof(struct info_serveur));
	int pip_rec[2];
	int pip_em[2];

	pipe(pip_rec);
	pipe(pip_em);	
	s->pipe_serveur_reception = pip_rec[0];
	s->pipe_serveur_emission = pip_em[1];

	s->pid = fork();
	if(!s->pid) {		
		sock_server(port,pip_rec[1],pip_em[0]);
	}

	s->tete_lecture = 0;
	s->tete_ecriture = 0;
	return s;
}

void fermer_serveur(Serveur s) {
	kill(s->pid,9);
	free(s);	
}

void gerer_messages(Serveur s) {
	char buff[LONGUEUR_MESSAGE]= {0};
	
	// nous faisons l'hypothèse que la taille d'un message du serveur n'excèdera jamais LONGUEUER_MESSAGE
	read(s->pipe_serveur_reception,buff,LONGUEUR_MESSAGE);	
	
	// un retour chariot est envoyé avant le message utile
	// je retire ce retour chariot
	if(buff[strlen(buff)-1] == '\n')
		buff[strlen(buff)-1] = '\0';
	if(strlen(buff) > 0){
		printf("%s\n",buff+1);
		ajouter_message_hist(s, buff+1);
	}
}

void envoyer_message_tous(Serveur s, char ** message) {
	int i = 0;
	char message_complet[1024] = {0};
	char * space = " ";

	while(message[i] != NULL) {
		strcat(message_complet,space);
		strcat(message_complet,message[i]);
		i++;
	}

	write(s->pipe_serveur_emission,message_complet, strlen(message_complet));
}

void ajouter_message_hist(Serveur s, char * message) {
	s->historique[s->tete_ecriture] = (char *)malloc(strlen(message));
	strncpy(s->historique[s->tete_ecriture],message,strlen(message));
	
	s->tete_ecriture ++;
	if(s->tete_ecriture == NOMBRE_MAX_MESSAGE)
		s->tete_ecriture = 0;
	if(s->tete_lecture == s->tete_ecriture) {
		s->tete_lecture ++;
		if(s->tete_lecture == NOMBRE_MAX_MESSAGE)
			s->tete_lecture = 0;
	}
}

void afficher_hist(Serveur s) {
	int i = s->tete_lecture;
	while(i != s->tete_ecriture) {
		printf("%s\n",s->historique[i]);
		i++;
		if(i==NOMBRE_MAX_MESSAGE)
			i = 0;
	}
}
