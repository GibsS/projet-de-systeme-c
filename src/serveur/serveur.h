#ifndef _SERVER
#define _SERVER

#define LONGUEUR_MESSAGE 1024
#define NUM_PORT 7000
#define NOMBRE_MAX_MESSAGE 10

typedef struct  info_serveur {
	int pid; // pid du processus faisant tourner le serveur

	// fd au travers du quelon peut recevoir les messages du serveur
	int pipe_serveur_reception;
	// fd au travers duquel on peut envoyer des messages au serveur
	int pipe_serveur_emission;
		
	// le rang du plus vieux messages encore enregistrés dans "historique"
	int tete_lecture; 
	// le rang du plus récent messages enregistrés dans "historique"
	int tete_ecriture;
	// le tableau des messages 
	char * historique[NOMBRE_MAX_MESSAGE];

} * Serveur;

// créer un serveur
Serveur creer_serveur();

// ferme le serveur s
void fermer_serveur(Serveur s);

// lis les messages reçu par le serveur et rend compte 
// de certain changement
void gerer_messages(Serveur s);

// envoie un message à tous les utilisateurs connectés
void envoyer_message_tous(Serveur s, char ** message);

// affiche les dix derniers messages envoyés par les clients
void afficher_hist(Serveur s);
#endif
