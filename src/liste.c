#include "liste.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Liste creer() {
	return NULL;
}

int inserer_en_tete(char** commande, int pid, int fd,  ETAT_PROC etat, Liste * liste) {
	Liste p = (Liste)malloc(sizeof(struct chainon));
	
	if(p == NULL) {
		printf("echec de malloc\n");
		return 0;
	} else {
	
	p->commande = (char *)malloc(200);
	if(!strcasecmp(commande[0],"nt")) {  
		strcpy(p->commande,commande[2]);
		strcat(p->commande," (xterm)");
	} else {
		strcpy(p->commande,commande[0]);
	}

	p->pid = pid;
	p->etat = etat;
	p->suivant = *liste;
	p->fd = fd;
	*liste = p;
	return 1;
	}
}

int inserer_en_queue(char ** commande, int pid, int fd, ETAT_PROC etat, Liste * liste) {
	Liste p;

	if(*liste == NULL) {
		*liste = (Liste)malloc(sizeof(struct chainon));

		(*liste)->commande = (char *)malloc(200);
		if(!strcasecmp(commande[0],"nt")) {
			strcpy((*liste)->commande,commande[1]);
			strcat((*liste)->commande," (xterm)");
		} else {
			strcpy((*liste)->commande,commande[0]);
		}

		(*liste)->pid = pid;
		(*liste)->etat = etat;
		(*liste)->suivant = NULL;
		(*liste)->fd = fd;
		
		return 1;
	} else {
		p = *liste;
		while(p->suivant != NULL) {
			p = p->suivant;
		}
		
		p->suivant = (Liste)malloc(sizeof(struct chainon));
		
		p->suivant->commande = (char *)malloc(200);
		if(!strcasecmp(commande[0],"nt")) {
			strcpy(p->suivant->commande,commande[1]);
			strcat(p->suivant->commande," (xterm)");
		} else {
			strcpy(p->suivant->commande,commande[0]);
		}

		p->suivant->pid = pid;
		p->suivant->etat = etat;
		p->suivant->suivant = NULL;
		p->suivant->fd = fd;
		return 1;
	}
}

ETAT_PROC lire_etat_tete(Liste l) {
	if(l == NULL) {
		return INEXISTANT;
	} else {
		return l->etat;
	}
}

int modifier(int pid, ETAT_PROC etat, Liste liste) {
	Liste p = liste;

	while(p != NULL && p->pid != pid) {
		p = p->suivant;
	}

	if(p != NULL) {
	  	p->etat = etat;
		return 1;
	} else {
		return 0;
	}
}

void afficher(Liste liste) {
	Liste p = liste;
	while(p != NULL) {
	  	printf("nom : %s pid : %d fd : %d " , p->commande, p->pid, p->fd);

		switch(p->etat) {
			case INEXISTANT : printf("etat : INEXISTANT \n");break;
			case ACTIF	: printf("etat : ACTIF \n");break; 
			case SUSPENDU 	: printf("etat : SUSPENDU \n");break;
		}
	  	p = p->suivant;
	}
}

ETAT_PROC lire_etat_proc(int pid, Liste liste) {
	Liste p = liste;

	while(p != NULL && p->pid != pid) {
		p = p->suivant;
	}

	if(p != NULL) {
	    	return p->etat;
	} else {
		return INEXISTANT;
	}
}

int lire_fd_proc(int pid, Liste liste) {
	Liste p = liste;

	while(p != NULL && p->pid != pid) {
		p = p->suivant;
	}

	if(p != NULL) {
		return p->fd;
	} else {
		return -1;
	}
}

int rechercher(int pid, Liste liste) {
	Liste p = liste;

	while(p != NULL && p->pid != pid) {
		p = p->suivant;
	}

	return p != NULL;
}

int supprimer_proc(int pid, Liste * liste) {
	Liste p = *liste;
	Liste p1= NULL;
	int est_present = 0;

	if(*liste != NULL) {
		if((*liste)->pid == pid) {
	  		*liste = (*liste)->suivant;
			if(p->fd != 0 && p->fd != 1 && p->fd != -1)
				close(p->fd);
			free(p->commande);
			free(p);
			est_present = 1;
		} else {
			while(p->suivant != NULL && p->suivant->pid != pid) {
				p = p->suivant;
			}
			if(p->suivant != NULL) {
				p1 = p->suivant;
				p->suivant = p->suivant->suivant;
				if(p1->fd != 0 && p1->fd != 1 && p1->fd != -1)
					close(p1->fd);
				free(p1->commande);
				free(p1);
				est_present = 1;
			}
		}
	}
	return est_present;
}

void detruire(Liste * liste) {
  	Liste p = *liste,q;

	while(p != NULL) {
		q = p;
		p = p->suivant;
		free(q);
	}

	*liste = NULL;
}
