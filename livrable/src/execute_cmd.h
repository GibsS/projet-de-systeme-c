#ifndef _EXECUTE_CMD
#define _EXECUTE_CMD

#include "liste.h"

// exécuter une commande dans un fichier avec <
void executer_cmd_vers_fichier1(char ** argv, int nombre_mot, Liste * l);

// exécuter une commande dans un fichier avec >
void executer_cmd_vers_fichier2(char ** argv, int nombre_mot, Liste * l);

// exécuter une commande dans un autre terminal
void executer_cmd_autre_terminal(char ** argv, int nombre_mot, Liste * l);

// exécuter une commande normalement
void executer_cmd(char ** argv, int nombre_mot, Liste * l);
#endif
