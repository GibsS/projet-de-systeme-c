#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#define MAX_MESSAGE 10

// inialise les variables utilisés
void init(char ** argv);
// s'occupe de lire en continu le fichier et constaté 
// un quelconque changement. Si une nouvelle ligne apparait,
// elle est rajouté à l'historique 
void recevoir_message();
// affiche les MAX_MESSAGE derniers messages
void afficher_message();
// ajouter un string à la liste de message
void ajouter_message_hist(char * message);

// les messages a affiché sont stockés dans un tableau. Le premier élément
// à afficher serait au rang tete_lecture, Le dernier à tete_ecriture -1 % MAX_MESSAGE
int tete_lecture;
int tete_ecriture;
char * historique[MAX_MESSAGE];

FILE* file;

int main(int argc, char ** argv) {
	init(argv);

	recevoir_message();	
}

void init(char ** argv) {
	file = fopen("hist","r");
	tete_lecture = 0;
	tete_ecriture =0;
}

void recevoir_message() {
	char texte[1024];
	fd_set fds;
	struct timeval zero = {0,0};

	while(1) {
		char * ret = fgets(texte,sizeof(texte),file);
		if(ret != NULL) {
			if(strcasecmp("\n",texte)) {
				ajouter_message_hist(texte);
				afficher_message();	
			}
		}
	}

}

void afficher_message() {
	int i = tete_lecture;
	
	system("clear");
        while(i != tete_ecriture) {
                printf("%s\n",historique[i]);
                i++;
                if(i== MAX_MESSAGE)
                        i = 0;
        }
}

void ajouter_message_hist(char * message) {
	printf("%s\n",message);
        historique[tete_ecriture] = (char *)malloc(strlen(message));
        strncpy(historique[tete_ecriture],message,strlen(message));
        
        tete_ecriture ++;
        if(tete_ecriture == MAX_MESSAGE)
                tete_ecriture = 0;
        if(tete_lecture == tete_ecriture) {
                tete_lecture ++;
           	if(tete_lecture == MAX_MESSAGE)
                        tete_lecture = 0;
        }
}

