#include "cmds.h"

#include "execute_cmd.h"
#include "liste.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

void executer_cmd_vers_fichier2(char ** argv, int nombre_mot, Liste * l) {
	int pid;
	int reussi = 1;
	int fd = open(argv[nombre_mot-1], O_CREAT|O_WRONLY,S_IRWXU|S_IRWXG|S_IRWXO);

	if(fd == -1) {
		printf("le fichier n'éxiste pas ou n'est pas lisible\n");
	} else {	
		pid = fork();
		switch(pid) {
			case -1 : printf("echec de fork()\n");
				  reussi = 0;
				  break;
			case 0  : dup2(fd,fileno(stdout));
				  argv[nombre_mot-2] = NULL;
				  if(execvp(argv[0],argv) == -1) {
					printf("échec d'execvp\n");
					reussi = 0;
					exit(-1);
				  }
				  break;
		}
		if(reussi)
			inserer_en_queue(argv,pid,-1,ACTIF,l);	
	}
}

void executer_cmd_vers_fichier1(char ** argv, int nombre_mot, Liste * l) {
	int pid;
	int reussi = 1;
	int fd = open(argv[0],O_CREAT|O_WRONLY,S_IRWXU|S_IRWXG|S_IRWXO);
	
	if(fd == -1) {
		printf("le fichier n'éxiste pas ou n'est pas lisible\n");
	} else {
		pid = fork();
		switch(pid) {
			case -1 : printf("echec de fork()\n");
				  reussi = 0;
				  break;
			case 0  : dup2(fd,fileno(stdout));
				  if(execvp(argv[2],argv+2) == -1) {
					printf("échec d'execvp\n");
					reussi = 0;
					exit(-1);
				  }
				  break;
		}
		if(reussi)
			inserer_en_queue(argv,pid,-1,ACTIF,l);
	}
}

void executer_cmd_autre_terminal(char ** argv, int nombre_mot, Liste * l) {
	char * new_argv[MAX_ARG+2];
	int i;
	int reussi = 1;
	int pid = fork();
	
	switch(pid) {
		case -1 : printf("échec de fork()\n");
			  reussi = 0;
			  break;
		case 0  : new_argv[0] = "xterm";
       			  new_argv[1] = "-e";
	        	  for(i = 0; i < MAX_ARG-1; i++) {
	           	  	new_argv[i+2] = argv[i+1];
		          }
			  if(execvp(new_argv[0],new_argv)==-1) {
				printf("échec d'execvp\n");
				reussi = 0;
				exit(-1);
			  }
			  break;
	}
	if(reussi)
		inserer_en_queue(argv,pid,-1,ACTIF,l);
}

void executer_cmd(char ** argv, int nombre_mot, Liste * l) {
	int pip[2];
	int pid;
	int reussi = 1;

	if(!pipe(pip)) {
		pid = fork();

		switch(pid) {	
			case -1 : printf("échec de fork()\n");
				  reussi = 0;
				  break;
			case 0  : close(pip[0]);
				  dup2(pip[1],fileno(stdout));
			   	  if(execvp(argv[0],argv)==-1) {
					printf("échec d'execvp\n");
					reussi = 0;
					exit(-1);
				  }
				  break;
			case 1 : close(pip[1]);
		}
		if(reussi)
			if(!inserer_en_queue(argv,pid,pip[0],ACTIF,l))
				printf("échec de l'insertion dûe à un manque de mémoire\n");
	} else {
		printf("echec de pipe()\n");
	}
}
