#include "cmds.h"

#include "serveur.h"
#include "liste.h"
#include "execute_cmd.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <unistd.h>

// Suit les changements d'états des différentes entrées et appelle les fonctions de traitement
// pour chacune d'entre elle
void traitement_entree();
// Lit une commande au clavier, l'interprète et lance la commande adéquate
void traitement_cmd();
// Parcourt l'ensemble des processus fils et vérifie leur état
void traitement_processus();
// Affiche les instructions d'utilisation d'utilisation
void afficher_instruction();
// Affiche la liste de processus fils
void afficher_processus();
// Arrête le programme principal
void fermer();

// Liste des processus
Liste liste_processus;

// Serveur de communication
Serveur serveur;

int main() {
	liste_processus = creer();
	
	afficher_instruction();
	while(1) {
		traitement_processus();
		traitement_entree();	
	}
}

void traitement_entree() {
	int entree_disponible;
	fd_set fds;
	struct timeval time_zero = {0,0};
	Liste p = liste_processus;
	char texte[1024] = {0};
	
	// Remise à zéro
	FD_ZERO(&fds);
	// Rajout des "input" à suivre
	// .. les processus fils
	while(p != NULL) { 
		if(p->fd != -1){ 
			FD_SET(p->fd,&fds);
		}
		p = p->suivant;
	}
	// .. les messages du serveur
	if(serveur != NULL) {
		FD_SET(serveur->pipe_serveur_reception,&fds);
	}
	// .. le clavier
	FD_SET(fileno(stdin),&fds);

	entree_disponible = select(FD_SETSIZE,&fds,NULL,NULL,&time_zero);
	
	// Traitement des entrées
	if(entree_disponible) {
		// .. clavier
		if(FD_ISSET(fileno(stdin),&fds)) {
			traitement_cmd();
		}
		// .. serveur
		if(serveur != NULL && FD_ISSET(serveur->pipe_serveur_reception,&fds)) {
			gerer_messages(serveur);
		}
		// .. les processus fils
		p = liste_processus;	
		while(p != NULL) {
			if(p->fd != -1 && FD_ISSET(p->fd,&fds)) {
				int n;
				bzero(texte,1024);
				while((n = read(p->fd,texte,1024))>0){	
					printf("%s",texte);fflush(stdout);
					bzero(texte,1024);
					//printf("%d\n",n);
					if(n < 1024)break;
				}
			} 
			p = p->suivant;
		}
	}
}

void traitement_cmd() {
	char * argv[MAX_ARG];
	int nombre_mot;
		
	nombre_mot = lire_cmd(MAX_ARG,argv);

	if(nombre_mot == 0) { 
		// réafficher le manuel
		afficher_instruction();
	} else if(!strcasecmp(argv[0],"kill")) { 
		// envoyer un signal
		if(argv[1] == NULL) {
			printf("Mauvaise syntaxe : kill <signal> <pid>\n");
		} else if (argv[2] == NULL) {
			kill(atoi(argv[1]),9);
		} else {
			kill(atoi(argv[2]),atoi(argv[1]));
		}
	} else if(!strcasecmp(argv[0],"ms")) { 
		// créer un serveur
		if(argv[1] == NULL) {	
			serveur = creer_serveur(NUM_PORT);
		} else {
			serveur = creer_serveur(atoi(argv[1]));
		}
	} else if(!strcasecmp(argv[0],"fermer_ms")) {
		// fermer le serveur si il existe
		if(serveur == NULL) {
			printf("Le serveur n'est pas lancé\n");			
		} else {
			fermer_serveur(&serveur);
		}
	} else if(!strcasecmp(argv[0],"env")) { 
		// envoyer un message à tous les clients connectés
		if(serveur != NULL) {
			envoyer_message_tous(serveur,argv+1);
		} else {
			printf("Le serveur n'est pas lancé\n");
		}
	} else if(!strcasecmp(argv[0],"hist")) { 
		// afficher la liste des 10 derniers messages
		if(serveur != NULL) {
			historique(serveur);
		} else {
			printf("Le serveur n'est pas lancé\n");
		}
	} else if(!strcasecmp(argv[0],"fermer_hist")) {
		// fermer le terminal d'historique
		fermer_hist(serveur);
	} else if(!strcasecmp(argv[0],"exit")) {
		// arrêter correctement le programme
		fermer();
	} else if(!strcasecmp(argv[0],"lp")) {
		// afficher la liste des processus
		afficher_processus();
	} else if(nombre_mot >= 3 && !strcasecmp(argv[1],"<")) {
		// executer la commande à droite de "<" et met le résultat dans le fichier à 
		// gauche de "<"
		executer_cmd_vers_fichier1(argv,nombre_mot,&liste_processus);
	} else if(nombre_mot >= 3 && !strcasecmp(argv[nombre_mot-2],">")) {
		// executer la commande à gauche de ">" et met le résultat dans le fichier à 
		// droite de ">"
		executer_cmd_vers_fichier2(argv,nombre_mot,&liste_processus);
	} else if(!strcasecmp(argv[0],"nt")) {
		// executer une commande dans un autre terminal
		executer_cmd_autre_terminal(argv,nombre_mot,&liste_processus);
	} else {
		// executer une commande normalement
		executer_cmd(argv,nombre_mot,&liste_processus);
	}
	liberer_cmd(nombre_mot, argv);
}

void traitement_processus() {
        Liste l = liste_processus;
        int status;
        int pid;
        int ret;

	int fd;
	char buff[1024];
	fd_set fds;
	struct timeval time_zero = {0,0};

        while(l != NULL) {
		pid = l->pid;
                ret = waitpid(pid,&status, WNOHANG|WUNTRACED|WCONTINUED);
		fd = l->fd;	
                l = l->suivant;
                if(ret){
                        if(WIFEXITED(status)) {
				// libération des données du pipe avant de le fermer 
				// (il y a un risque que le processus s'arrête avant que son output soit traité)
				FD_ZERO(&fds);
				FD_SET(fd,&fds);
				select(FD_SETSIZE,&fds,NULL,NULL,&time_zero);
				if(FD_ISSET(fd,&fds)) {
					read(fd,buff,1024);
					printf("%s",buff);
				}				
				// signaler utilisateur
                                printf("pid %d s'est terminé normalement\n",pid);
                                printf("status : %d\n", status);
                                supprimer_proc(pid,&liste_processus);
                        }
                        if(WIFSIGNALED(status)) {
				// libération des données du pipe avant de le fermer
				FD_ZERO(&fds);
				FD_SET(fd,&fds);
				select(FD_SETSIZE,&fds,NULL,NULL,&time_zero);
				if(FD_ISSET(fd,&fds)) {
					read(fd,buff,1024);
					printf("%s",buff);
				}
				// signaler utilisateur
                                printf("pid %d s'est terminé dû au signal : %d\n", pid,WTERMSIG(status));
                                printf("status : %d\n", status);
                                supprimer_proc(pid,&liste_processus);
                        }
                        if(WSTOPSIG(status)) {
                                printf("pid %d est suspendu dû au signal : %d\n", pid,WSTOPSIG(status));
                                modifier(pid,SUSPENDU,liste_processus);
                        }
                        if(WIFCONTINUED(status)) {
                                printf("pid %d a été relancé \n,pid",pid);
                                modifier(pid,ACTIF,liste_processus);
                        }
                }
        }
}
	
void afficher_instruction() {
        printf("----- Menu des commandes-----------\n"
        "- une commande externe quelconque\n"
        "- commande > fichier ou fichier < commande (les resultats de commande sont diriges vers fichier)\n"
        "- \"nt\" commande (dans un autre terminal)\n"
        "- \"lp\" (affiche liste des processus)\n"
        "- \"ms\" num_port (lance un serveur de communication sur le port num_port)\n"
	"- \"fermer_ms\" (fermer le serveur de communication)\n"
        "- \"env\" num_connexion message(envoie message vers la connexion indiquee)\n"
        "- \"kill\" num_sig num_proc\n"
        "- \"hist\" (affiche l'historique des echanges)\n" 
	"- \"fermer_hist\" (fermer l'historique de communication)\n"
        "- \"exit\" (arret) -- la touche return affiche le menu -- \n");
}

void afficher_processus() {
	printf("Liste de processus : \n");
	afficher(liste_processus);
}

void fermer() {
	Liste p = liste_processus;

	// Tuer l'ensemble des processus fils
	while(p != NULL) {
		kill(p->pid,9);
		supprimer_proc(p->pid,&liste_processus);
		p = p->suivant;
	}		
	// fermer le serveur si il y en a un
	if(serveur != NULL) {
		fermer_serveur(&serveur);
	}

	exit(0);
}
